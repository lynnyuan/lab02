import styles from './CustomersPage.module.css';
import CustomerTable from './CustomerTable';
import { useState, useEffect } from 'react';
import { initialCustomers } from './data';
import { Switch, Route, useRouteMatch } from 'react-router-dom';
import NewCustomerDialog from './NewCustomerDialog';
import createPersistedState from'use-persisted-state';

const useCustomerState = createPersistedState('data');

export default function CustomersPage({onAddCustomer, onCancelNewCustomer}) {

    const [customers, setCustomers] = useState(initialCustomers);
    const [data, setData] = useCustomerState(customers);
    const { url, path } = useRouteMatch();

    useEffect(()=>{
            setData(customers)
        
    }, [customers])

    return (
        <>
                <Switch>
                    <Route path={`${path}/add`}>
                        <NewCustomerDialog onAddCustomer={onAddCustomer} 
                        onCancelNewCustomer={onCancelNewCustomer} 
                        initialCustomers = {initialCustomers}/>
                    </Route>
                </Switch>
            <main>
                <div className="box">
                    <CustomerTable customers={customers} />
                </div>
            </main>
        </>
    )
}