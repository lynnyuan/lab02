import {useState} from 'react'
import Modal from './Modal';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';

var dayjs = require('dayjs')

function NewCustomerDialog({onCancelNewCustomer, onAddCustomer}) {

    const [firstName, setFirstName ] = useState('');
    const [lastName, setLastName ] = useState('');
    const [dob, setDob] = useState('');
    const [isActive, setActive] = useState('')
    const [tier, setTier] = useState('Platinum')
    const [expireDay, setExpireDay] = useState('')

    function handleDOB(){
        var selected = dayjs(document.getElementById("dob").value)
        setDob(
            selected
        )
    }

    function handleIsActive(){
        var isActive = document.getElementById("isActive").value
        setActive(
            isActive
        )
    }

    function handleTier(){
        var tier = document.getElementById("tier").value
        setTier(
            tier
        )
    }

    function handleExpireDate(){
        var expires = dayjs(document.getElementById("expireDate").value)
        setExpireDay(
            expires
        )
    }

    return (
        <Modal style={{ width: '50%', height: 'auto' }} dismissOnClickOutside={true}
        onCancel = {onCancelNewCustomer}>
            <h2>Add Customer</h2>
            <div>
                <label>First Name:</label>
                <input type="text" value={firstName} onInput={e => setFirstName(e.target.value)} /> 
                <br/>
                <label>Last Name:</label>
                <input type="text" value={lastName} onInput={e => setLastName(e.target.value)} /> 
                <br/>
                {/* <label>DoB</label> */}
                {/* <input type = "date" id = 'dob' onSelect = {()=>{handleDOB()}}></input> */}
                <TextField
                        id="dob"
                        label="Birthday"
                        type="date"
                        defaultValue="2017-05-24"
                        onSelect = {()=>{handleDOB()}}
                        InputLabelProps={{
                        shrink: true,
                        }}
                    />
                    <br/>
                    <label>Is Active</label>
                    <select id="isActive" onChange = {()=>{handleIsActive()}}>
                        <option value="Yes">Yes</option>
                        <option value="No">No</option>
                    </select>
                    <br/>
                    <label>Tier</label>
                    <select id="tier" onChange = {()=>{handleTier()}}>
                        <option value="Platinum" selected>Platinum</option>
                        <option value="Gold">Gold</option>
                        <option value="Silver">Silver</option>
                        <option value="Bronze">Bronze</option>
                    </select>
                    <br/>
                     <label>Expire day</label> 
                    <input type = "date" id = 'expireDate' onSelect = {()=>{handleExpireDate()}}></input> 
                
            </div>
            <Button variant="contained" color="primary" onClick = {() => onAddCustomer(firstName, lastName, dob,isActive, tier, expireDay)}>Add</Button>
        </Modal>

    )
}

export default NewCustomerDialog
