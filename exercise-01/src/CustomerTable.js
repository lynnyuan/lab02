import styles from './CustomerTable.module.css';
import dayjs from 'dayjs';
import SortButton from './SortButton';
import { useEffect, useState } from 'react';
import { sortCustomers, isActive } from './data-sort-utils';
import { Link } from 'react-router-dom';
import Button from '@material-ui/core/Button';
import createPersistedState from'use-persisted-state';

const useCustomerState = createPersistedState('data');

function makeSortButton(prop, sortProperty, sortDirection, onClick) {
    return (
        <SortButton prop={prop}
            mode={sortProperty === prop ? sortDirection : 'none'}
            onClick={onClick} />
    );
}

export default function CustomerTable({ customers, onAddCustomer, onCancleNewCustomer }) {
    const [data, setData] = useCustomerState(customers);
    const [sortProperty, setSortProperty] = useState('none');
    const [sortDirection, setSortDirection] = useState('none');

    const handleSortButtonClick = (prop, newSortDirection) => {
        setSortProperty(newSortDirection === 'none' ? 'none' : prop);
        setSortDirection(newSortDirection);
    }
    const sortedCustomers = sortCustomers(data, sortProperty, sortDirection);

    // const [data, setData] = useLocalStorage('data',sortedCustomers)
    // useEffect(()=>{
    //     if(sortProperty !== 'none' || sortDirection !== 'none'){
    //         setData(sortedCustomers)
    //     }
    // }, [sortedCustomers])

    console.log(data)
    
    useEffect(()=>{
        if(sortProperty !== 'none' || sortDirection !== 'none'){
            setData(sortedCustomers)
        }
    }, [sortProperty, sortDirection])
    
    return (
        <div>
        <table className={styles.table}>
            <thead>
                <tr>
                    <th colSpan={4} className={styles.borderRight}>Personal details</th>
                    <th colSpan={3}>Membership details</th>
                </tr>
                <tr>
                    <th>ID {makeSortButton('id', sortProperty, sortDirection, handleSortButtonClick)}</th>
                    <th>First name {makeSortButton('firstName', sortProperty, sortDirection, handleSortButtonClick)}</th>
                    <th>Last name {makeSortButton('lastName', sortProperty, sortDirection, handleSortButtonClick)}</th>
                    <th className={styles.borderRight}>Date of birth {makeSortButton('dob', sortProperty, sortDirection, handleSortButtonClick)}</th>
                    <th>Active? {makeSortButton('isActive', sortProperty, sortDirection, handleSortButtonClick)}</th>
                    <th>Tier {makeSortButton('membershipTier', sortProperty, sortDirection, handleSortButtonClick)}</th>
                    <th>Expiry date {makeSortButton('membershipExpires', sortProperty, sortDirection, handleSortButtonClick)}</th>
                </tr>
            </thead>
            <tbody>
                {data.map(customer => (
                    <tr key={customer.id} className={isActive(customer) ? styles.active : styles.inactive}>
                        <td>{customer.id}</td>
                        <td>{customer.firstName}</td>
                        <td>{customer.lastName}</td>
                        <td className={styles.borderRight}>{dayjs(customer.dob).format('ll')}</td>
                        <td>{isActive(customer) ? 'Yes' : 'No'}</td>
                        <td>{customer.membershipTier}</td>
                        <td>{dayjs(customer.membershipExpires).calendar()}</td>
                    </tr>
                ))}
            </tbody>
        </table>
            <Link to = {`customers/add`}><Button variant="contained" color="primary">Add new customer</Button></Link>
        </div>
    );
}