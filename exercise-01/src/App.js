import { Switch, Route, Redirect, useHistory } from 'react-router-dom';
import { useState} from 'react';
import { initialCustomers } from './data';
import NavBar from './NavBar';
import CustomersPage from './CustomersPage';
import createPersistedState from'use-persisted-state';
import {AppContext} from './app-context'

/**
 * Renders a navbar allowing the user to browse to the articles or gallery pages.
 * If the user tries to browse to any other URL, they are auto-redirected to the articles page.
 */

function App() {

  const history = useHistory();
  // const [customers, setCustomers] = useState(initialCustomers);

  const useCustomerState =createPersistedState('data');

  const [data, setData] = useCustomerState();

  function handleAddCustomer(firstName, lastName, dob, isActive, tier , expireDay){

    // const updatedCustoemers = [...customers]
    const updatedCustomers = [...data]
    
    const newCustomer = {
      id: data.length + 1,
      firstName: firstName,
      lastName: lastName,
      dob: dob,
      membershipTier: tier,
      membershipExpires: expireDay
    }

    updatedCustomers.push(newCustomer)
    setData(updatedCustomers)
    
    //need to return the view with the new state
    setData(updatedCustomers)
    history.goBack();
  }

  function handleCancelNewCustomer(){
    history.goBack();
  }

  return (
    <div className="container">
    <AppContext.Provider value={[data, setData]}>
      <nav>
        <NavBar />
      </nav>
      </AppContext.Provider>

      <Switch>
        <Route path="/customers">
          <CustomersPage onAddCustomer = {handleAddCustomer}
          onCancelNewCustomer = {handleCancelNewCustomer}/>
        </Route>

        <Route path="*">
          <Redirect to="/customers" />
        </Route>
      </Switch>

    </div>
  );
}

export default App;